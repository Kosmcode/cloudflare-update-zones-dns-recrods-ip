import logging
import requests
import os
import json
import CloudFlare

CONFIG_FILE_PATH = 'config.json'
URL_ACTUAL_IP_ADDRESS = 'https://api.ipify.org'
FILEPATH_IP_LAST_RESPONSE = './ip_last_response'
FILEPATH_LOG = './cloudflare_update.log'
DNS_RECORD_TYPE_A = 'A'

logging.basicConfig(
    format='%(asctime)s [%(levelname)s] - %(message)s',
    filename=FILEPATH_LOG,
    level=logging.INFO,
)
logger = logging.getLogger()


def is_change_ip_address(actual_ip):
    if not os.path.isfile(FILEPATH_IP_LAST_RESPONSE):
        actual_ip_last_response_handler = open(FILEPATH_IP_LAST_RESPONSE, 'w')
        actual_ip_last_response_handler.write(actual_ip)
        actual_ip_last_response_handler.close()

        return True

    actual_ip_last_response_handler = open(FILEPATH_IP_LAST_RESPONSE, 'r')
    last_ip_address = actual_ip_last_response_handler.read()

    if actual_ip == last_ip_address:
        return False

    actual_ip_last_response_handler = open(FILEPATH_IP_LAST_RESPONSE, 'w')
    actual_ip_last_response_handler.write(actual_ip)
    actual_ip_last_response_handler.close()

    return True


def get_actual_ip_address():
    actual_ip_request = requests.get(url=URL_ACTUAL_IP_ADDRESS, params={'format': 'json'})

    if actual_ip_request.status_code != 200:
        logger.error('Actual IP address response error')

        exit(1)

    if not actual_ip_request.json()['ip']:
        logger.error('Actual IP address response not has IP')

        exit(1)

    logger.info('Actual IP: {}'.format(actual_ip_request.json()['ip']))

    return actual_ip_request.json()['ip']


def get_config_data():
    if not os.path.isfile(CONFIG_FILE_PATH):
        logger.error('Config file not exists')

        exit(1)

    config_file = open(CONFIG_FILE_PATH, 'r')

    return json.load(config_file)


actual_ip_address = get_actual_ip_address()

if not is_change_ip_address(actual_ip_address):
    logger.info('IP address not changed')

    exit(0)

logger.info('IP address changed - updating')

config = get_config_data()

zone_names_to_update = config['zoneNamesToUpdate']

cloudflare = CloudFlare.CloudFlare(email=config['email'], key=config['globalApiKey'])

zones = cloudflare.zones.get()

for zone in zones:
    if not zone['name'] in zone_names_to_update:
        continue

    logger.info("Updating zone: {}".format(zone['name']))

    zone_id = zone['id']

    dns_records = cloudflare.zones.dns_records.get(zone_id)

    for dns_record in dns_records:
        if dns_record['type'] != DNS_RECORD_TYPE_A:
            continue

        logger.info("Updating zone record dns: {}".format(dns_record['name']))

        result = cloudflare.zones.dns_records.put(
            zone_id,
            dns_record['id'],
            data={
                "content": actual_ip_address,
                "name": dns_record['name'],
                "type": DNS_RECORD_TYPE_A
            }
        )

logger.info("Updated")
