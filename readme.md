# Python script to update Zones DNS IP on cloudflare

## Usage

1. Install required packages
```bash
pip install -r requirements.txt
```

2. Copy example env and set websites data
```bash
cp config.example.json config.json
```

3. Run script
```bash
python3 cloudflare_update_dns_ip.py
```

ProTrick: Add this script to crontab on server